
-- this file configure the cloakrooms on the map

local cfg = {}

-- prepare surgeries customizations
local surgery_male = { model = "mp_m_freemode_01" }
local surgery_female = { model = "mp_f_freemode_01" }

-- sheriff models
local uniform_sheriff1 = { model = "s_m_y_sheriff_01" }
local uniform_fsheriff1 = { model = "s_f_y_sheriff_01" }

--Security models
local uniform_security1 = { model = "s_m_m_security_01" }
local uniform_security2 = { model = "s_m_m_armoured_01" }
local uniform_security3 = { model = "s_m_m_armoured_02" }

local uniform_swat = { model = "s_m_y_swat_01"}

for i=0,19 do
  surgery_female[i] = {0,0}
  surgery_male[i] = {0,0}
end

-- cloakroom types (_config, map of name => customization)
--- _config:
---- map_entity: {ent,cfg} will fill cfg.title, cfg.pos
---- permissions (optional)
---- not_uniform (optional): if true, the cloakroom will take effect directly on the player, not as a uniform you can remove
cfg.cloakroom_types = {
  ["police"] = {
    _config = { permissions = {"police.cloakroom"}, map_entity = {"PoI", {marker_id = 1}} },
    ["Male uniform"] = {
      [3] = {30,0},
      [4] = {25,2},
      [6] = {24,0},
      [8] = {58,0},
      [11] = {55,0},
      ["p2"] = {2,0}
    },
    ["Female uniform"] = {
      [3] = {35,0},
      [4] = {30,0},
      [6] = {24,0},
      [8] = {6,0},
      [11] = {48,0},
      ["p2"] = {2,0}
    }
  },
  ["emergency"] = {
    _config = { permissions = {"emergency.cloakroom"}, map_entity = {"PoI", {marker_id = 1}} },
    ["Male uniform"] = {
      [3] = {81,0},
      [4] = {0,0},
      [8] = {15,0},
      [6] = {42,0},
      [11] = {26,0},
      ["p0"] = {6,1},
      ["p6"] = {12,1}
    }
  },
  ["jail"] = {
    _config = { map_entity = {"PoI", {marker_id = 1}} },
    ["Male suit"] = {
      [3] = {5,0},
      [4] = {7,15},
      [8] = {5,0},
      [6] = {12,6},
      [11] = {5,0}
    }
  },
  ["sheriff"] = {
    _config = { permissions = {"sheriff.cloakroom"} },
    ["Male Sheriff"] = uniform_sheriff1,
    ["Female Sheriff"] = uniform_fsheriff1,
    ["SWAT"] = uniform_swat
  },
  ["security"] = {
    _config = { permissions = {"security.cloakroom"} },
    ["Security General Patrol"] = uniform_security1,
    ["Armoured Security"] = uniform_security2,
    ["Armoured Security 2"] = uniform_security3
  },
  ["surgery"] = {
    _config = { not_uniform = true, map_entity = {"PoI", {marker_id = 1}} },
    ["Male"] = surgery_male,
    ["Female"] = surgery_female
  }
}

cfg.cloakrooms = {
  {"police", 454.324096679688,-991.499938964844,30.689577102661},
  {"emergency", -498.472290039063,-332.419097900391,34.5017356872559},
  --{"jail",450.048889160156,-990.477600097656,30.6896018981934},
  {"surgery",1849.7425,3686.5759,34.2670},
  {"sheriff", -448.20059204102,6007.8999023438,31.716388702393},
  {"security",-227.28967285156,6333.0786132813,32.423797607422}
}

return cfg
