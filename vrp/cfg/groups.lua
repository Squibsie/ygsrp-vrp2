
local cfg = {}

-- define each group with a set of permissions
-- _config property:
--- title (optional): group display name
--- gtype (optional): used to have only one group with the same gtype per player (example: a job gtype to only have one job)
--- onspawn (optional): function(user) (called when the character spawn with the group)
--- onjoin (optional): function(user) (called when the character join the group)
--- onleave (optional): function(user) (called when the character leave the group)

function police_init(user)
  local weapons = {}
  weapons["WEAPON_STUNGUN"] = {ammo=1000}
  weapons["WEAPON_COMBATPISTOL"] = {ammo=100}
  weapons["WEAPON_PUMPSHOTGUN"] = {ammo=50}
  weapons["WEAPON_NIGHTSTICK"] = {ammo=0}
  weapons["WEAPON_FLASHLIGHT"] = {ammo=0}

  vRP.EXT.PlayerState.remote._giveWeapons(user.source,weapons,true)
  vRP.EXT.Police.remote._setCop(user.source,true)
  vRP.EXT.PlayerState.remote._setArmour(user.source,100)
end

function security_init(user)
  local weapons = {}
  weapons["WEAPON_STUNGUN"] = {ammo=1000}
  weapons["WEAPON_PISTOL"] = {ammo=100}
  weapons["WEAPON_NIGHTSTICK"] = {ammo=0}
  weapons["WEAPON_FLASHLIGHT"] = {ammo=0}

  vRP.EXT.PlayerState.remote._giveWeapons(user.source,weapons,true)
  vRP.EXT.Police.remote._setCop(user.source,true)
  vRP.EXT.PlayerState.remote._setArmour(user.source,100)
end

function police_onjoin(user)
  police_init(user)
end

function user_onjoin(user)
  vRP.EXT.Police.remote._setCop(user.source,true) --Disables ingame wanted level for all players.
end

function police_onleave(user)
  vRP.EXT.PlayerState.remote._giveWeapons(user.source,{},true)
  vRP.EXT.Police.remote._setCop(user.source,false)
  vRP.EXT.PlayerState.remote._setArmour(user.source,0)
  user:removeCloak()
end

function police_onspawn(user)
  police_init(user)
end

function security_onjoin(user)
  security_init(user)
end

function security_onspawn(user)
  security_init(user)
end

cfg.groups = {
  ["superadmin"] = {
    _config = {onspawn = function(user) vRP.EXT.Base.remote._notify(user.source, "You are superadmin.") end},
    "player.group.add",
    "player.group.remove",
    "player.givemoney",
    "player.giveitem"
  },
  ["admin"] = {
    "admin.tickets",
    "admin.announce",
    "player.list",
    "player.whitelist",
    "player.unwhitelist",
    "player.kick",
    "player.ban",
    "player.unban",
    "player.noclip",
    "player.custom_emote",
    "player.custom_model",
    "player.custom_sound",
    "player.display_custom",
    "player.coords",
    "player.tptome",
    "player.tpto"
  },
  ["god"] = {
    "admin.god" -- reset survivals/health periodically
  },
  -- the group user is auto added to all logged players
  ["user"] = {
    _config = {
      onjoin = user_onjoin
    },
    "player.phone",
    "player.calladmin",
    "player.store_weapons",
    "police.seizable" -- can be seized
  },
  ["police"] = {
    _config = {
      title = "Los Santos Police",
      gtype = "job",
      onjoin = police_onjoin,
      onspawn = police_onspawn,
      onleave = police_onleave
    },
    "police.menu",
    "police.askid",
    "police.cloakroom",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
    "police.vehicle",
    "police.chest_seized",
    "-player.store_weapons",
    "-police.seizable" -- negative permission, police can't seize itself, even if another group add the permission
--    "mission.paycheck.police" -- basic mission
  },
  ["sheriff"] = {
    _config = {
      title = "Paleto Sheriff",
      gtype = "job",
      onjoin = police_onjoin,
      onspawn = police_onspawn,
      onleave = police_onleave
    },
    "police.menu",
    "police.askid",
    "sheriff.cloakroom",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
    "police.vehicle",
    "police.chest_seized",
    "sheriff.gunshop",
    "-player.store_weapons",
    "-police.seizable" -- negative permission, police can't seize itself, even if another group add the permission
--    "mission.paycheck.police" -- basic mission
  },
  ["Security"] = {
    _config = {
      title = "State Licensed Security",
      gtype = "job",
      onjoin = security_onjoin,
      onspawn = security_onspawn,
      onleave = police_onleave
    },
    "police.menu",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "-police.store_weapons",
    "-police.seizable", -- negative permission, police can't seize itself, even if another group add the permission
    "security.office",
    "security.cloakroom"
  },
  ["BountyHunter"] = {
    _config = {
      title = "Fugitive Recovery Agent",
      gtype = "job",
      onjoin = security_onjoin,
      onspawn = security_onspawn,
      onleave = police_onleave
    },
    "police.menu",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "-police.store_weapons",
    "-police.seizable" -- negative permission, police can't seize itself, even if another group add the permission
  },
  ["fireems"] = {
    _config = {
      title = "Fire/EMS",
      gtype = "job"
    },
    "emergency.revive",
    "emergency.shop",
    "emergency.service",
    "emergency.vehicle",
    "emergency.cloakroom"
  },
  ["mechanic"] = {
    _config = {
      title = "Mechanic",
      gtype = "job"
    },
    "vehicle.repair",
    "vehicle.replace",
    "repair.service"
--    "mission.repair.satellite_dishes", -- basic mission
--    "mission.repair.wind_turbines" -- basic mission
  },
  ["taxi"] = {
    _config = {
      title = "Taxi",
      gtype = "job"
    },
    "taxi.service",
    "taxi.vehicle"
  },
  ["courier"] = {
    _config = {
      title = "Courier",
      gtype = "job"
    },
    "courier.depot"
  },
  ["citizen"] = {
    _config = {
      title = "Citizen",
      gtype = "job"
    }
  }
}

-- groups are added dynamically using the API or the menu, but you can add group when an user join here
cfg.users = {
  [1] = { -- give superadmin and admin group to the first created user on the database
    "superadmin",
    "admin"
  }
}

-- group selectors
-- _config
--- x,y,z, map_entity, permissions (optional)
---- map_entity: {ent,cfg} will fill cfg.title, cfg.pos

cfg.selectors = {
  ["Jobs"] = {
    _config = {x = -268.363739013672, y = -957.255126953125, z = 31.22313880920410, map_entity = {"PoI", {blip_id = 351, blip_color = 47, marker_id = 1}}},
    "taxi",
    "mechanic",
    "citizen"
  },
  --[["Police job"] = {
    _config = {x = 437.924987792969,y = -987.974182128906, z = 30.6896076202393, map_entity = {"PoI", {blip_id = 351, blip_color = 38, marker_id = 1}}},
    "police",
    "citizen"
  },]]
  ["Emergency job"] = {
    _config = {x=-498.959716796875,y=-335.715148925781,z=34.5017547607422, map_entity = {"PoI", {blip_id = 351, blip_color = 1, marker_id = 1}}},
    "fireems",
    "citizen"
  }
}

-- identity display gtypes
-- used to display gtype groups in the identity
-- map of gtype => title
cfg.identity_gtypes = {
  job = "Job"
}

return cfg
