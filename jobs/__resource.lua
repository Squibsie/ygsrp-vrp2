description "Jobs for YGSRP"

dependencies{
  "vrp"
}

server_scripts{
  "@vrp/lib/utils.lua",
  "courier/courierServer.lua"
}

client_scripts {
  "courier/courierClient.lua"
}
