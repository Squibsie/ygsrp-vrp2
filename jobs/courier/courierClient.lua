local workStarted = 0
local totalRuns = 0

local courierBlip
local courierMarker


--Table holding Routes for Courier runs in Paleto.
  local PaletoRoutes = {
      {x = -423.92620849609,y = 6031.5932617188,z = 30.793573379517},
      {x = -778.58435058594,y = 5574.71875,z = 32.957321166992},
      {x = -707.94860839844,y = 5802.6953125,z = 16.896999359131},
      {x = -456.76782226563,y = 6352.7749023438,z = 11.875594139099},
      {x = -397.37911987305,y = 6059.2250976563,z = 30.97829246521},
      {x = -355.43518066406,y = 6068.8701171875,z = 30.972593307495},
      {x = -252.2078704834,y = 6146.1430664063,z = 30.824363708496},
      {x = -140.65745544434,y = 6202.8237304688,z = 30.687311172485},
      {x = 44.907691955566,y = 6300.8266601563,z = 30.704015731812},
      {x = 204.60336303711,y = 6379.9580078125,z = 30.917329788208},
      {x = 145.08264160156,y = 6643.154296875,z = 31.008890151978},
      {x = -65.760765075684,y = 6537.2353515625,z = 30.966110229492},
      {x = -90.230567932129,y = 6471.7197265625,z = 30.775115966797},
      {x = -217.33166503906,y = 6355.6469726563,z = 30.965726852417},
      {x = 1429.6340332031,y = 6337.5751953125,z = 23.991987228394},
      {x = 1506.8935546875,y = 6329.8168945313,z = 23.466836929321},
      {x = 1745.3002929688,y = 6403.3486328125,z = 35.222923278809},
      {x = 1694.3634033203,y = 6427.8745117188,z = 32.10192489624},
      {x = 423.072265625,y = 6520.6127929688,z = 27.182497024536},
      {x = 2.0737302303314,y = 6444.6181640625,z = 30.898899078369},
      {x = -24.52822303772,y = 6459.2211914063,z = 30.889974594116},
      {x = -59.022731781006,y = 6466.4184570313,z = 30.81566619873},
      {x = -47.711196899414,y = 6476.9189453125,z = 30.732122421265},
      {x = -17.481527328491,y = 6507.3930664063,z = 30.765707015991},
      {x = 0.16773223876953,y = 6524.9516601563,z = 30.765768051147},
      {x = 44.876586914063,y =6589.3579101563,z = 30.813018798828},
      {x = 24.53373336792,y =6568.7495117188,z = 30.801675796509},
      {x = 5.5996398925781,y =6549.9633789063,z = 30.803142547607},
      {x = -141.67752075195,y =6437.5678710938,z = 30.906461715698},
      {x = 51.945854187012,y =6640.6381835938,z = 30.967742919922},
      {x = 26.219816207886,y =6651.1596679688,z = 30.89623260498},
      {x = -9.1912336349487,y =6639.98046875,z = 30.549291610718},
      {x = -38.283657073975,y =6625.6674804688,z = 29.685319900513},
      {x = -120.78130340576,y =6554.306640625,z = 28.90470123291},
      {x = -212.1130065918,y =6433.1831054688,z = 30.889091491699},
      {x = -228.65858459473,y =6415.185546875,z = 30.824481964111},
      {x = -264.58410644531,y =6395.3237304688,z = 30.296358108521},
      {x = -353.98776245117,y =6328.8618164063,z = 29.379299163818},
      {x = -328.30276489258,y =6326.4213867188,z = 29.646230697632},
      {x = -388.20761108398,y =6298.83984375,z = 28.931722640991},
      {x = -414.53686523438,y =6258.0317382813,z = 29.880216598511},
      {x = -419.79577636719,y =6208.8735351563,z = 30.967136383057},
      {x = -339.80697631836,y =6159.7548828125,z = 30.966373443604},
      {x = -839.89337158203,y =5409.33984375,z = 33.97216796875},
      {x = -577.43829345703,y =5252.1240234375,z = 69.946212768555},
      {x = -1493.9881591797,y =4981.3935546875,z = 62.567317962646},
      {x = -108.31508636475,y =6313.056640625,z = 30.965078353882},
      {x = -150.28884887695,y =6310.1918945313,z = 30.863899230957} -- 48 Locations.
    }

function drawTxt(text, font, centre, x, y, scale, r, g, b, a)
    	SetTextFont(font)
    	SetTextProportional(0)
    	SetTextScale(scale, scale)
    	SetTextColour(r, g, b, a)
    	SetTextDropShadow(0, 0, 0, 0, 255)
    	SetTextEdge(1, 0, 0, 0, 255)
    	SetTextDropShadow()
    	SetTextOutline()
    	SetTextCentre(centre)
    	SetTextEntry("STRING")
    	AddTextComponentString(text)
    	DrawText(x, y)
end

local function createCourierVehicle(vehiclehash)
  RequestModel(vehiclehash)
  Citizen.CreateThread(function()
      local waiting = 0
      while not HasModelLoaded(vehiclehash) do
          waiting = waiting + 100
          Citizen.Wait(100)
          if waiting > 5000 then
              ShowNotification("~r~ERROR: Vehicle could not load in time.")
              break
          end
        end
    local courierVan = (CreateVehicle(vehiclehash, -422.07043457031,6127.5864257813,31.376886367798, 220.0, 1, 0))
  end)
end

RegisterNetEvent("courier:startCourierRun")
AddEventHandler("courier:startCourierRun", function (source, player)
  --Test Server function to handlle markers/blips.
  local route = math.random(1,48) -- Second number denotes max locations.
  local x = PaletoRoutes[route].x
  local y = PaletoRoutes[route].y
  local z = PaletoRoutes[route].z
  workStarted = 1
  TriggerEvent("courier:setRouteMarker",player,x,y,z,source)
  Citizen.Trace("courier:startCourierRun called")
  totalRuns = totalRuns+1
  Citizen.Trace("totalRuns: " .. totalRuns)

end)

RegisterNetEvent("courier:setRouteMarker")
AddEventHandler("courier:setRouteMarker", function(player,x,y,z,source)
  courierBlip = (AddBlipForCoord(x,y,z))
  SetBlipSprite(courierBlip, 1)
  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Courier Drop")
  EndTextCommandSetBlipName(courierBlip)
  --courierMarker = DrawMarker(1,x,y,z - 1,0,0,0,0,0,0,2.0001,2.0001,0.7001,255,125,125,150,0,0,0,0)
  courierCheckpoint = (CreateCheckpoint(1, x, y, z, 0, 0, 0, 1.0001, 255, 255, 255, 150, 0))

  Citizen.CreateThread(function()
    --Test Distance to courierMarker
    while workStarted == 1 do
      Citizen.Wait(5)
      if GetDistanceBetweenCoords(x,y,z,GetEntityCoords(GetPlayerPed(-1),0)) < 3.0 then
        if IsVehicleModel(GetVehiclePedIsIn(GetPlayerPed(-1), true), GetHashKey("boxville2"))  then
          drawTxt('Press ~g~E~s~ to deliver your ~b~ package', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
          if (IsControlJustReleased(1, 38)) then
            TriggerServerEvent("sendMissionText", "Delivering... Please Wait.")
            Citizen.Wait(5000)
            workStarted = 0
            x = 0
            y = 0
            z = 0
            RemoveBlip(courierBlip)
            DeleteCheckpoint(courierCheckpoint)
						courierDropSuccess(player)
					end
        end
      end
    end
  end)
  Citizen.Trace("courier:setRouteMarker called")
end)

RegisterNetEvent("courier:setWorkStarted")
AddEventHandler("courier:setWorkStarted", function (ws)
  Citizen.Trace("courier:setWorkStarted called")
  workStarted = ws
end)

RegisterNetEvent("courier:isWorkStarted")
AddEventHandler("courier:isWorkStarted", function (source)
  Citizen.Trace("courier:isWorkStarted called")
  if workStarted == 0 then
    Citizen.Trace("courier workstarted == 0")
    createCourierVehicle("boxville2")
    local player = GetPlayerFromServerId(source)
    Citizen.Trace("source: " .. source)
    Citizen.Trace("player: " .. player)
    TriggerEvent("courier:startCourierRun", source, player)
  else
    local player = GetPlayerFromServerId(source)
    TriggerServerEvent("courier:errorWorkInProgress", source)
  end
end)

RegisterNetEvent("courier:stopWork")
AddEventHandler("courier:stopWork", function (source)
  if workStarted == 1 then
    workStarted = 0
    totalRuns = 0
    RemoveBlip(courierBlip)
    DeleteCheckpoint(courierCheckpoint)
    SetEntityAsMissionEntity(courierVan, true, true)
    DeleteVehicle(courierVan)
    TriggerServerEvent("sendMissionText", "Work Stopped. You may need to move/delete your van from the spawn.")
  else
    TriggerServerEvent("sendMissionText", "You need to be working first!")
  end
end)

function courierDropSuccess(player)
  if totalRuns ~= 15 then -- Number of runs before return to depot.
    TriggerEvent("courier:startCourierRun", player)
    TriggerServerEvent("sendMissionText","Make the next delivery!")
  else
    courierBlip = (AddBlipForCoord(-400.06268310547,6168.6435546875,31.009784698486))
    SetBlipSprite(courierBlip, 1)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString("Courier Depot")
    EndTextCommandSetBlipName(courierBlip)
    TriggerServerEvent("sendMissionText","Return to the Depot.")
    courierCheckpoint = (CreateCheckpoint(1, -400.06268310547,6168.6435546875,31.009784698486, 0, 0, 0, 1.0001, 255, 255, 255, 150, 0))
    returnDepot = 1

    Citizen.CreateThread(function()
      --Test Distance to courierMarker
      while returnDepot == 1 do
        Citizen.Wait(5)
        if GetDistanceBetweenCoords(-400.06268310547,6168.6435546875,31.009784698486,GetEntityCoords(GetPlayerPed(-1),0)) < 3.0 then
          if IsVehicleModel(GetVehiclePedIsIn(GetPlayerPed(-1), true), GetHashKey("boxville2"))  then
            drawTxt('Press ~g~E~s~ to return your vehicle', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
            if (IsControlJustReleased(1, 38)) then
              returnDepot = 0
              totalRuns = 0
              RemoveBlip(courierBlip)
              DeleteCheckpoint(courierCheckpoint)
              local ped = GetPlayerPed(-1)
              local courierVehicle = GetVehiclePedIsIn(ped, false)
              Citizen.Trace("courierVehicle: " .. courierVehicle)
              SetEntityAsMissionEntity(courierVehicle, true, true)
              DeleteVehicle(courierVehicle)
              TriggerServerEvent("courier:runSuccess")
            end
          end
        end
      end
    end)
  end
  end
