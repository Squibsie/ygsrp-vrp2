--Call for VRP API interface
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","courier")

local function ch_StartWork(player,choice)
  local user_id = vRP.getUserId(player)
  TriggerClientEvent("courier:isWorkStarted", player, user_id)
end
    -- location to spawn work truck
    -- Select a route.
    -- issue first marker and set GPS.
    -- When reached, issue next marker.
    -- On route completion. Pay 500 dollars.

local function ch_StopWork(player,choice)
  local user_id = vRP.getUserId(player)
  TriggerClientEvent("courier:stopWork", player)
end


local jobMenu = {
name = "PostOp Courier",
css = {top = "75px", header_color = "rgba(0,255,0,0.75)"},
onclose = function(player)
  print('menu closed')
end
}
jobMenu["Start Work"] = {ch_StartWork}
jobMenu["Stop Work"] = {ch_StopWork, "Stop work with no pay."}

--[[This job is intended to be a basic short distance courier role, whereby you
are tasked with doing a set 'run' of 15 stops before being paid. The job will be
undertaken in a boxville. A completed run will give you 500 dollars. There will
be no entry or progression in this role.
Author: Squibsie
]]

local function depotEnter(player)

  local user_id = vRP.getUserId(player)
  if vRP.hasPermission(user_id, "courier.depot") then
    vRP.openMenu(player,jobMenu)
  end
end

local function depotLeave(player,area_name)
  vRP.closeMenu(player,jobMenu)
end

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn)
  TriggerClientEvent("courier:setWorkStarted", source, 0)
  Citizen.Trace("courier:setWorkStarted called")
  if first_spawn then
    --Add Courier depot
    vRPclient._addBlip(source,-405.16616821289,6150.0083007813,31.678293228149,478,4,"PostOp Paleto Depot")
    vRPclient._addMarker(source,-405.16616821289,6150.0083007813,30.678293228149,0.7,0.7,0.5,0,255,125,125,150)
    vRP.setArea(source,"PostOpDepot",-405.16616821289,6150.0083007813,31.678293228149,1,1.5,depotEnter,depotLeave)
  end
end)


RegisterNetEvent("courier:errorWorkInProgress")
AddEventHandler("courier:errorWorkInProgress", function(player)
    vRPclient._notify(player,"You are already working! Find your truck by the gate!")
end)

RegisterNetEvent("sendMissionText")
AddEventHandler("sendMissionText", function(msgtext)
    Citizen.Trace("sendMissionText source: " .. source .. msgtext .. "\n")
    local reciept = source
    vRPclient.notify(reciept,msgtext)
end)

RegisterNetEvent("courier:runSuccess")
AddEventHandler("courier:runSuccess", function()
    Citizen.Trace("player: " .. source)
    local reciept = source
    local user_id = vRP.getUserId(reciept)
    vRP.giveMoney(user_id,500)
    vRPclient.notify(reciept,"You have completed the run and been paid $500.")
end)
